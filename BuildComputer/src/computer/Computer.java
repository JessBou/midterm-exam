package computer;
class PC {
	Monitor theMonitor;
	Case theCase;
	Motherboard theMotherboard;
	PC(Monitor theMonitor, Case theCase, Motherboard theMotherboard) {
		this.theMonitor = theMonitor;
		this.theCase = theCase;
		this.theMotherboard = theMotherboard;
	}
	public void pressPowerButton() {
		theCase.getThecase();
	}
}
class Monitor {
	private String model;
	private String manufacturer;
	private int size;
	private Resolution resolution;
	Monitor(String model, String manufacturer, int size, Resolution resolution) {
		this.model = model;
		this.manufacturer = manufacturer;
		this.size = size;
		this.resolution = resolution;
	}
	public String getModel() {
		return model;
	}
	public String getmanufacturer() {
		return manufacturer;
	}
	public int getSize() {
		return size;
	}
	public Resolution getResolution() {
		return resolution;
	}
}
class Resolution {
	int a;
	int b;
	Resolution(int a, int b) {
		this.a = a;
		this.b = b;
	}
}
class Case {
	private String model;
	private String manufacturer;
	private String powerSupply;
	private Dimensions dimension;
	Case(String model, String manufacturer, String powerSupply, Dimensions dimension) {
		this.model = model;
		this.manufacturer = manufacturer;
		this.powerSupply = powerSupply;
		this.dimension = dimension;
	}
	public String getModel() {
		return model;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public String getPowerSupply() {
		return powerSupply;
	}
	public Dimensions getDimensions() {
		return dimension;
	}
	public void getThecase() {
		System.out.println("Power Button Pressed");
	}
}
class Dimensions {
	int a;
	int b;
	int c;
	Dimensions(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
}
class Motherboard {
	private String model;
	private String manufacturer;
	private int ramSlots;
	private int cardSlots;
	private String bios;
	Motherboard (String model, String manufacturer, int ramSlots, int cardSlots, String bios) {
		this.model = model;
		this.manufacturer = manufacturer;
		this.ramSlots = ramSlots;
		this.cardSlots = cardSlots;
		this.bios = bios;
	}
	public String getModel() {
		return model;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public int getramSlots() {
		return ramSlots;
	}
	public int getcardSlots() {
		return cardSlots;
	}
	public String getBios() {
		return bios;
	}
}
public class Computer {

	public static void main(String[] args) {
		Dimensions dimensions = new Dimensions(20, 20, 5);
		Case theCase = new Case("220B", "Dell", "240", dimensions);
		Resolution resolution = new Resolution(2540, 1440);
		Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27, resolution);
		Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");
		PC thePC = new PC(theMonitor, theCase, theMotherboard);
		thePC.pressPowerButton();

	}

}
