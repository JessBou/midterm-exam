package bank;
abstract class Bank1 {
	abstract double getBalance();
}
class BankA extends Bank1 {
	double balance;
	BankA(double balance) {
		this.balance = balance;
	}
	@Override
	double getBalance() {
		return balance;
		
	}
	
}
class BankB extends Bank1 {
	double balance;
	BankB(double balance) {
		this.balance = balance;
	}
	@Override
	double getBalance() {
		return balance;
		
	}
}
class BankC extends Bank1 {
	double balance;
	BankC(double balance) {
		this.balance = balance;
	}
	@Override
	double getBalance() {
		return balance;
		
	}
}
public class Bank {

	public static void main(String[] args) {
		BankA deposit = new BankA(100);
		System.out.println("Bank A balance: " + deposit.getBalance());
		BankB deposit2 = new BankB(150);
		System.out.println("Bank B balance: " + deposit2.getBalance());
		BankC deposit3 = new BankC(200);
		System.out.println("Bank C balance: " + deposit3.getBalance());

	}

}
