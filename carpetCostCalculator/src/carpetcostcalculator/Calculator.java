package carpetcostcalculator;
class Floor {
	double width;
	double length;
	Floor(double width, double length) {
		this.width = width;
		this.length = length;
	}
	double getArea() {
		double area = width * length;
		return area;
	}
}
class Carpet {
	double cost;
	Carpet(double cost) {
		this.cost = cost;
	}
	double getCost() {
		return cost;
	}
}
class Calculator1 {
	Floor floor;
	Carpet carpet;
	Calculator1(Floor floor, Carpet carpet) {
		this.floor = floor;
		this.carpet = carpet;
	}
	double getTotalCost() {
		double totalCost = floor.getArea() * carpet.getCost();
		return totalCost;
	}
}
public class Calculator {

	public static void main(String[] args) {
		Carpet carpet = new Carpet(3.5);
		Floor floor = new Floor(2.75, 4.0);
		Calculator1 calculator = new Calculator1(floor, carpet);
		System.out.println("total: " + calculator.getTotalCost());
		System.out.println("====================================");
		carpet = new Carpet(1.5);
		floor = new Floor(5.4, 4.5);
		calculator = new Calculator1(floor, carpet);
		System.out.println("total: " + calculator.getTotalCost());

	}

}
